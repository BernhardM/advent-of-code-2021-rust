def day01():
    with open ('input', 'r') as f:
        lines = f.readlines()

    counter = 0
    for idx, line in enumerate(lines):
        if idx > 0 and int(line) > int(lines[idx-1]):
            counter = counter + 1

    print(counter)

    counter2 = 0
    for idx, line in enumerate(lines):
        if idx > (len(lines) - 4):
            print(counter2)
            break
        if (int(lines[idx+1]) + int(lines[idx+2]) + int(lines[idx+3])) > (int(lines[idx]) + int(lines[idx+1]) + int(lines[idx+2])):

            counter2 = counter2 +1

    print(counter2)


if __name__ == '__main__':
    day01()

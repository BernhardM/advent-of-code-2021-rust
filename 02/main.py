def day02():
    with open ('input', 'r') as f:
        lines = f.readlines()

    depth = 0
    forw = 0
    aim = 0

    for line in lines:
        if 'forward' in line:
            x = int(line[8:9])
            forw = forw + x
            depth = depth + (aim * x)
        elif 'down' in line:
            x = int(line[5:6])
            aim = aim + x
        elif 'up' in line:
            x = int(line[3:4])
            aim = aim - x

    print(depth)
    print(forw)
    print(depth*forw)
    print(aim)


if __name__ == '__main__':
    day02()
